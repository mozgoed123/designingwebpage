const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

const validateLogin = () => {
    const email = document.getElementById("email");
    if (email.value === "") {
        email.setCustomValidity("Email is empty");
        return false;
    }
    if (!email.value.match(emailRegex)) {
        email.setCustomValidity("Please enter a valid email address");
        return false;
    }

    const password = document.getElementById("password");
    if (password.value === "") {
        password.setCustomValidity("Password field must be filled out");
        return false;
    }
    if (password.value.length < 8) {
        password.setCustomValidity("Password length must be more than 8 symbols");
        return false;
    }

    return true;
};

const validateSignup = () => {
    const username = document.getElementById("username");
    if (username.value === "") {
        username.setCustomValidity("Username is empty");
        return false;
    }
    if (username.value.length < 5) {
        username.setCustomValidity("Username length must be more than 4 symbols");
        return false;
    }

    const email = document.getElementById("email");
    if (email.value === "") {
        email.setCustomValidity("Email is empty");
        return false;
    }
    if (!email.value.match(emailRegex)) {
        email.setCustomValidity("Please enter a valid email address");
        return false;
    }

    const password = document.getElementById("password");
    if (password.value === "") {
        password.setCustomValidity("Password field must be filled out");
        return false;
    }
    if (password.value.length < 8) {
        password.setCustomValidity("Password length must be more than 8 symbols");
        return false;
    }

    const confirmPassword = document.getElementById("confirm-password");
    if (confirmPassword.value === "") {
        confirmPassword.setCustomValidity("Confirm password field must be filled out too");
        return false;
    }
    if (confirmPassword.value !== password.value) {
        confirmPassword.setCustomValidity("Please ensure that the password and confirm password fields match.");
        return false;
    }

    return true;
};

const clearError = () => {
    const inputs = document.getElementsByTagName("input");
    for(let i = 0; i < inputs.length; i++) {
        inputs[i].setCustomValidity("");
    }
};